package eu.telecomnancy.sensor;


public class ConcreteCommandFahrenheit implements Command {

	public AbstractSensor abstractSensor;

	public ConcreteCommandFahrenheit(AbstractSensor a){
		this.abstractSensor = a;
	}
	
	@Override
	public void execute(){	
		try {
			//prepar to attach all abstractSensro's observers to the decorator :
			AbstractSensor tmpAbstractSensor = (AbstractSensor)Factory.build("FahrenheitDecorator", abstractSensor);
			tmpAbstractSensor.Notify();
		} catch (SensorNotActivatedException e) {
			System.out.println("SensorNotActivatedException in ConcreteCommandFahrenheit\n");
			e.printStackTrace();
		} catch (FactoryException e) {
			System.out.println("Factory exception in ConcreteCommandFahrenheit\n");
			e.printStackTrace();
		}	
	}

	
}