package eu.telecomnancy.sensor;


public class ConcreteCommandOff implements Command {

	public AbstractSensor abstractSensor;
	
	public ConcreteCommandOff(AbstractSensor a){
		this.abstractSensor = a;
	}
	
	@Override
	public void execute() {	
		abstractSensor.off();
		try {
			abstractSensor.Notify();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
