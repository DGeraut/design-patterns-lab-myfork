package eu.telecomnancy.sensor;

public interface TemperatureSensorState {
	
	public void update() throws SensorNotActivatedException;
	
	public double getValue() throws SensorNotActivatedException;
	
	public boolean getStatus();
}
