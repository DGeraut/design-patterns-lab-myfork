package eu.telecomnancy.sensor;


public class TemperatureSensor extends AbstractSensor {
    TemperatureSensorState state;

    

    @Override
    public void on() {
        state = new SensorON();
    }

    @Override
    public void off() {
        state = new SensorOFF();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }
    
    @Override
    public void Notify() {
    	for (int i = 0; i < linkedObserver.size(); i++) {
    		try {
				linkedObserver.get(i).update(state.getValue());
			} catch (SensorNotActivatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
	}

}
