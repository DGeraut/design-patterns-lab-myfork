package eu.telecomnancy.sensor;

public class FactoryException extends Exception{
	public FactoryException(String message) {
        super(message);
    }
}
