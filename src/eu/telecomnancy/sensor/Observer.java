package eu.telecomnancy.sensor;

public interface Observer {
		public void update(double value);
		
		public void updateFahrenheit(double x);
}
