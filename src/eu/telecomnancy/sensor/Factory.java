package eu.telecomnancy.sensor;

public class Factory {
	public static ISensor build (String name,ISensor inside) throws FactoryException{
		if (name == "Proxy"){
			return new Proxy((AbstractSensor)inside);
		}else if(name == "TemperatureSensor"){
			return new TemperatureSensor();
		}else if(name == "LegacySensorAdapter"){
			return new LegacySensorAdaptor((LegacyTemperatureSensor)inside);
		}else if(name == "FahrenheitDecorator"){
			return new FahrenheitDecorator((AbstractSensor)inside);
		}else if(name == "RoundDecorator"){
			return new RoundDecorator((AbstractSensor)inside);
		}else{
			throw new FactoryException("Nom inconnu");
		}
	}
	
	public static ISensor build (String name) throws FactoryException{
		if (name == "Proxy"){
			return new Proxy();
		}else if(name == "TemperatureSensor"){
			return new TemperatureSensor();
		}else{
			throw new FactoryException("Nom inconnu");
		}
	}
	
}
