package eu.telecomnancy.sensor;


public interface Subject {
	

	
	public void Attach(Observer obs);
	
	public void Detach(Observer obs);
	
	public void Notify()throws SensorNotActivatedException;
}
