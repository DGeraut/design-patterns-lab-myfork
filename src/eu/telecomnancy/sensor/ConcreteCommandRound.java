package eu.telecomnancy.sensor;


public class ConcreteCommandRound implements Command {

	public AbstractSensor abstractSensor;

	public ConcreteCommandRound(AbstractSensor a){
		this.abstractSensor = a;
	}
	
	@Override
	public void execute(){	
		try {
			//prepar to attach all abstractSensro's observers to the decorator :
			AbstractSensor tmpAbstractSensor = (AbstractSensor)Factory.build("RoundDecorator", abstractSensor);
			tmpAbstractSensor.Notify();
		} catch (SensorNotActivatedException e) {
			System.out.println("SensorNotActivatedException in ConcreteCommandFahrenheit\n");
			e.printStackTrace();
		} catch (FactoryException e) {
			System.out.println("Factory exception in ConcreteCommandFahrenheit\n");
			e.printStackTrace();
		}	
	}

	
}