package eu.telecomnancy.sensor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Proxy extends AbstractSensor{
	public AbstractSensor abstractSensor;
	private File log = new File("../log.txt");
	
	public Proxy(AbstractSensor a){
		this.abstractSensor = a;
		this.linkedObserver = a.linkedObserver;
	}
	
	public Proxy(){
		try {
			this.abstractSensor = (AbstractSensor)Factory.build("TemperatureSensor");
		} catch (FactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void on() {
		abstractSensor.on();
		System.out.println("proxy turned on\n");
		Date date = new Date();
		String save = date.toString() + ": Sensor turned on\n";
		try {
			FileWriter fop = new FileWriter(log,true);
			fop.write(save);
			fop.close();
		} catch (IOException e) {
			System.out.println("no log file detected or unwritable\n");
			e.printStackTrace();
		}
		

	}

	@Override
	public void off() {
		abstractSensor.off();
		System.out.println("proxy turned off\n");
		Date date = new Date();
		String save = date.toString() + ": Sensor turned off\n";
		try {
			FileWriter fop = new FileWriter(log,true);
			fop.write(save);
			fop.close();
		} catch (IOException e) {
			System.out.println("no log file detected or unwritable\n");
			e.printStackTrace();
		}
		

	}

	@Override
	public boolean getStatus() {
		System.out.println("proxy getStauts\n");
		return abstractSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		abstractSensor.update();
		System.out.println("proxy updated\n");
		Date date = new Date();
		String save = date.toString() + ": Sensor updated\n";
		try {
			FileWriter fop = new FileWriter(log,true);
			fop.write(save);
			fop.close();
		} catch (IOException e) {
			System.out.println("no log file detected or unwritable\n");
			e.printStackTrace();
		}
		

	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		
		System.out.println("proxy getValue\n");
		Date date = new Date();
		String save = date.toString() + ": Sensor returned value " + abstractSensor.getValue() +"\n";
		try {
			FileWriter fop = new FileWriter(log,true);
			fop.write(save);
			fop.close();
		} catch (IOException e) {
			System.out.println("no log file detected or unwritable\n");
			e.printStackTrace();
		}
		return abstractSensor.getValue();

	}

	@Override
	public void Notify() {
    	for (int i = 0; i < linkedObserver.size(); i++) {
    		try {
				linkedObserver.get(i).update(this.getValue());
			} catch (SensorNotActivatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
	}
}
