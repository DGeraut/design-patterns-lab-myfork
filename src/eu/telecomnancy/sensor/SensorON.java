package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorON implements TemperatureSensorState{
	
    double value = 0;

    
	public void update(){
		value = (new Random()).nextDouble() * 100;
	}
	
	public boolean getStatus(){
		return true;
	}
	
	public double getValue(){
		return value;
	}
	


}
