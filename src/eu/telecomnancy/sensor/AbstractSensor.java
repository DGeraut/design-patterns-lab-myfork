package eu.telecomnancy.sensor;

import java.util.LinkedList;

public abstract class AbstractSensor implements ISensor, Subject{
	
	public LinkedList<Observer> linkedObserver = new LinkedList<Observer>();
	
	@Override
	public void Attach(Observer obs) {
		linkedObserver.add(obs);
		
	}

	@Override
	public void Detach(Observer obs) {
		linkedObserver.remove(obs);
		
	}

	
	

}
