package eu.telecomnancy.sensor;
import java.util.LinkedList;

public class LegacySensorAdaptor extends AbstractSensor {

	private LegacyTemperatureSensor AdaptedSensor;
	public LegacySensorAdaptor ( LegacyTemperatureSensor a ){
		this.AdaptedSensor = a;

	}
	@Override
	public void on() {
		
		if ( AdaptedSensor.getStatus()==false ){
			AdaptedSensor.onOff();
		}
		
	}

	@Override
	public void off() {
		if ( AdaptedSensor.getStatus()==true ){
			AdaptedSensor.onOff();
		}
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(AdaptedSensor.getStatus()==true){
				//DO NOTHIG
		}else{
			SensorNotActivatedException error = new SensorNotActivatedException("senseur innactif\n");
			throw error;
		}
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (AdaptedSensor.getStatus()==true){
			return AdaptedSensor.getTemperature();
		}else{
			SensorNotActivatedException error = new SensorNotActivatedException("senseur innactif\n");
			throw error;
		}
		
	}
	@Override
	public boolean getStatus() {
		return AdaptedSensor.getStatus();
	}
	
	
	@Override
	public void Notify() {
    	for (int i = 0; i < linkedObserver.size(); i++) {
    		try {
				linkedObserver.get(i).update(this.getValue());
			} catch (SensorNotActivatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
	}
	
	

}
