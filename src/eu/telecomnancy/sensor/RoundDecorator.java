package eu.telecomnancy.sensor;

public class RoundDecorator extends Decorator {

	public RoundDecorator(AbstractSensor a){
		this.abstractSensor = a;
		this.linkedObserver = a.linkedObserver;
	}
	@Override
	public void on() {
		abstractSensor.on();
		
	}

	@Override
	public void off() {
		abstractSensor.off();
		
	}

	@Override
	public boolean getStatus() {
		return abstractSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		abstractSensor.update();
	}
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		return (Math.round(abstractSensor.getValue()));
	}
	@Override
	public void Notify() {
    	for (int i = 0; i < linkedObserver.size(); i++) {
    		try {
				linkedObserver.get(i).update(this.getValue());
			} catch (SensorNotActivatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
	}


}
