package eu.telecomnancy.sensor;


public class ConcreteCommandUpdate implements Command {

	public AbstractSensor abstractSensor;
	
	public ConcreteCommandUpdate(AbstractSensor a){
		this.abstractSensor = a;
	}
	
	@Override
	public void execute(){	
		try {
			abstractSensor.update();
			abstractSensor.Notify();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	
}