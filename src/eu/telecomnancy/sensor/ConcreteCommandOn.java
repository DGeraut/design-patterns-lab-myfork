package eu.telecomnancy.sensor;


public class ConcreteCommandOn implements Command {

	public AbstractSensor abstractSensor;
	
	public ConcreteCommandOn(AbstractSensor a){
		this.abstractSensor = a;
	}
	
	@Override
	public void execute() {	
		abstractSensor.on();
		try {
			abstractSensor.Notify();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}