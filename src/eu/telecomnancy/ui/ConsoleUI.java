package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ConcreteCommandFahrenheit;
import eu.telecomnancy.sensor.ConcreteCommandOff;
import eu.telecomnancy.sensor.ConcreteCommandOn;
import eu.telecomnancy.sensor.ConcreteCommandRound;
import eu.telecomnancy.sensor.ConcreteCommandUpdate;
import eu.telecomnancy.sensor.FahrenheitDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.RoundDecorator;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConsoleUI {

    private ISensor Sensor;
    private Scanner console;

    public ConsoleUI(ISensor sensor) {
        this.Sensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();

    }

    public void manageCLI() {
        String rep = "";
        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - fahrenheit|f: convert to fahrenheit - set round|sr: set the value round");
        while (!"q".equals(rep)) {
            try {
                System.out.print(":> ");
                rep = this.console.nextLine();
                if ("on".equals(rep) || "o".equals(rep)) {
                    this.Sensor.on();
                    System.out.println("sensor turned on.");
                } else if ("off".equals(rep) || "O".equals(rep)) {
                	 this.Sensor.off();
                    System.out.println("sensor turned off.");
                } else if ("status".equals(rep) || "s".equals(rep)) {
                    System.out.println("status: " + this.Sensor.getStatus());
                } else if ("update".equals(rep) || "u".equals(rep)) {
                    this.Sensor.update();
                    System.out.println("sensor value refreshed.");
                } else if ("value".equals(rep) || "v".equals(rep)) {
                    System.out.println("value: " + this.Sensor.getValue());
                } else if ("fahrenheit".equals(rep) || "f".equals(rep)) {
                	Sensor = new FahrenheitDecorator((AbstractSensor)Sensor);
                    System.out.println("value in fahrenheit.");
                } else if ("set round".equals(rep) || "r".equals(rep)) {
                	Sensor = new RoundDecorator((AbstractSensor)Sensor);
                    System.out.println("rounded value");
                }else if ("proxy".equals(rep) || "p".equals(rep)) {
                	Sensor = new Proxy((AbstractSensor)Sensor);
                        System.out.println("Proxy is on");
                } else {
                    System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - fahrenheit|f: convert to fahrenheit - set round|sr: set the value round");
                }
                
            } catch (SensorNotActivatedException ex) {
                Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
