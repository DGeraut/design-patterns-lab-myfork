package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Observer;
import eu.telecomnancy.sensor.Proxy;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {

    private AbstractSensor sensor;
    private SensorView sensorView;

    public MainWindow(ISensor sensor) {
        this.sensor = (AbstractSensor) sensor;
        this.sensorView = new SensorView(this.sensor);
        this.sensor.Attach(sensorView);

        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
