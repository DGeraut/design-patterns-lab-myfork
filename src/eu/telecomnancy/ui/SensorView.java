package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ConcreteCommandFahrenheit;
import eu.telecomnancy.sensor.ConcreteCommandOff;
import eu.telecomnancy.sensor.ConcreteCommandOn;
import eu.telecomnancy.sensor.ConcreteCommandRound;
import eu.telecomnancy.sensor.ConcreteCommandUpdate;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Observer;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements Observer {
    private ConcreteCommandOn cOn;
    private ConcreteCommandOff cOff;
    private ConcreteCommandUpdate cUpdate;
    private ConcreteCommandFahrenheit cFahrenheit;
    private ConcreteCommandRound cRound;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton fahrenheit = new JButton("Fahrenheit");
    private JButton round = new JButton("Round value");

    public SensorView(ISensor c) {
    	this.cOn = new ConcreteCommandOn((AbstractSensor)c);
    	this.cOff = new ConcreteCommandOff((AbstractSensor)c);
    	this.cUpdate = new ConcreteCommandUpdate((AbstractSensor)c);
    	this.cFahrenheit = new ConcreteCommandFahrenheit((AbstractSensor)c);
    	this.cRound = new ConcreteCommandRound((AbstractSensor)c);
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cOn.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	cOff.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
            	cUpdate.execute();
                    
            }
        });
        
        fahrenheit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cFahrenheit.execute();
            }
        });
        
        round.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cRound.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(fahrenheit);
        buttonsPanel.add(round);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
    public void update(double x){
    	value.setText(x + " °C");
    }
    
    public void updateFahrenheit(double x){
    	value.setText(x + " °F");
    }
}
